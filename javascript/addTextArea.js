function addTextArea(data,target) {
    const x = document.createElement("TEXTAREA");
    const t = document.createTextNode( typeof data == "string"?data:JSON.stringify(data));
    x.appendChild(t);
    if(!target) return x;
    const textArea=document.getElementById(target)
    if(!textArea) throw("target element not fouund for "+target)
    textArea.appendChild(x);
 }