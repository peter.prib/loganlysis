function createTables() {
    alasql('CREATE table IF NOT EXISTS track ('+
        'id int AUTOINCREMENT PRIMARY KEY,'+
        'x1 float, '+
        'y1 float, '+
        'x2 float, '+
        'y2 float, '+
        'x3 float, '+
        'y3 float '+
    ')')
    alasql('CREATE table IF NOT EXISTS log ('+
        'id int AUTOINCREMENT PRIMARY KEY,'+
        'episode int, '+
        'step int, ' +
        'x float, '+
        'y float, '+
        'heading float, '+
        'steering_angle float, '+
        'speed float, '+
        'action_taken, '+
        'reward float, '+
        'done, '+
        'all_wheels_on_track boolean, '+
        'current_progress  float, '+
        'closest_waypoint_index, '+
        'track_length  float, '+
        'time  float '+
    ')')
    alasql('CREATE table IF NOT EXISTS logdiffs ('+
        'id int AUTOINCREMENT PRIMARY KEY,'+
        'episode int, '+
        'step int, ' +
        'x float, '+
        'y float, '+
        'heading float, '+
        'steering_angle float, '+
        'speed float, '+
        'action_taken, '+
        'reward float, '+
        'current_progress  float '+
    ')')
}