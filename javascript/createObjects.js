function createObjects(){
  const result=alasql('\
  CREATE TABLE IF NOT EXISTS log ( \
    id INT PRIMARY KEY, \
    name STRING, \
    url STRING \
  ); \
  IF EXISTS (SELECT * FROM log WHERE Id=0) \
    UPDATE log SET name = "dummy", url = "url updated" WHERE Id=0 \
  ELSE \
    INSERT INTO log VALUES (0,"dummy","url"); \
  ')
}
function deleteObjects(){
  const result=alasql(' \
    drop table aaa; \
  ')
}