function parseLog(data,saveFunction,debug){
    if(data.substr(1,1)=="["){
      saveFunction(JSON.parse(data))
      return
    } 
    if(debug) alasql('SELECT top 333 * FROM CSV(?,{headers:false,separator:","}) ',[data],(data)=>{console.log("parslog ",data)})
    alasql('SELECT ' +
      '[0] as lineHeader, '+
      'SUBSTRING([0],0,14) as episode, '+
      '[1] as step, ' +
      '[2] as x, '+
      '[3] as y, '+
      '[4] as heading, '+
      '[5] as steering_angle, '+
      '[6] as speed, '+
      '[7] as action_taken, '+
      '[8] as reward, '+
      '[9] as done, '+
      '[10] as all_wheels_on_track, '+
      '[11] as current_progress, '+
      '[12] as closest_waypoint_index, '+
      '[13] as track_length, '+
      '[14] as time, '+
      '[15] as col15, '+
      '[16] as col16 '+
      'FROM CSV(?,{headers:false,separator:","}) WHERE [0] like \'SIM_TRACE_LOG:%\'  ',[data,'SIM_TRACE_LOG:'],saveFunction)
  }