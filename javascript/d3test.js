
<h1>test d3</h1>

<div id="d3container"></div>
<script type="module">
import * as d3 from "https://cdn.jsdelivr.net/npm/d3@7/+esm";
export function graphd3() {
  alasql('SELECT x,y INTO D3(\'d3container\') FROM (select id, x1 as x, y1 as y from track) order by id')
}
</script>
<input  type="button" value="graph D3" onclick="graphd3();"/>


function d3test(width = 960,height = 500) {
    const projection = d3.geo.mercator()
    .center([0, 5 ])
    .scale(200)
    .rotate([-180,0]);
    const svg = d3.select("body").append("svg")
    .attr("width", width)
    .attr("height", height);
    const path = d3.geo.path()
    .projection(projection);
    const g = svg.append("g");
    d3.json("https://unpkg.com/world-atlas@1.1.4/world/110m.json",function(error, topology) {
        alasql('SELECT * FROM xlsx("https://cdn.rawgit.com/agershun/alasql-org/gh-pages/demo/014map/cities.xlsx",{headers:true})',[], function(data) {
            g.selectAll("text")
            .data(data)
            .enter()
            .append('text')
            .text((d){return d.city})
            .attr("x", function(d) {
                return projection([d.lon, d.lat])[0];
            })
            .attr("y", function(d) {
            return projection([d.lon, d.lat])[1];
            })
        });
        g.selectAll("path")
        .data(topojson.object(topology, topology.objects.countries).geometries)
        .enter()
        .append("path")
        .attr("d", path)
    });
    const zoom = d3.behavior.zoom()
    .on("zoom",function() {
        g.attr("transform","translate("+ 
            d3.event.translate.join(",")+")scale("+d3.event.scale+")"
            );
        g.selectAll("circle")
        .attr("d", path.projection(projection));
        g.selectAll("path")  
        .attr("d", path.projection(projection)); 

    });
    svg.call(zoom)
}

alasql.into.D3 = function (selector, opts, data, columns, cb) {
	if (!selector) throw Error("no graph target")
	if (!data) throw Error("no data")
	if (data.length == 0) throw Error("no data")
	const d3container = typeof selector == 'string'?alasql.getElement(selector):selector
	
	const baseOpts ={width:640, height:400,marginTop:20,marginRight:20,marginBottom:30,marginLeft:40}
//	const x = d3.scaleUtc()
//		.domain([new Date("2023-01-01"), new Date("2024-01-01")])
//		.range([baseOpts.marginLeft, baseOpts.width - baseOpts.marginRight]);
	const x = d3.scaleLinear()
		.domain([0, 100])
		.range([marginLeft, width - marginRight]);
	const y = d3.scaleLinear() // Declare the y (vertical position) scale.
		.domain([0, 100])
		.range([baseOpts.height - baseOpts.marginBottom, baseOpts.marginTop]);
	const svg = d3.create("svg") // Create the SVG container.
		.attr("width", baseOpts.width)
		.attr("height", baseOpts.height);
	svg.append("g") // Add the x-axis.
		.attr("transform", `translate(0,${baseOpts.height - baseOpts.marginBottom})`)
		.call(d3.axisBottom(x));
	svg.append("g") // Add the y-axis.
		.attr("transform", `translate(${baseOpts.marginLeft},0)`)
		.call(d3.axisLeft(y));
	d3container.append(svg.node());

    const line = d3.line()
      .curve(d3.curveCatmullRom)
      .x(datapoint => x(datapoint.x))
      .y(datapoint => y(datapoint.y));
	const length = (path) => d3.create("svg:path").attr("d", path).node().getTotalLength()
	const d3dataLength = length(line(d3data));
	svg.append("path")
      .datum(d3data)
      .attr("fill", "none")
      .attr("stroke", "black")
      .attr("stroke-width", 2.5)
      .attr("stroke-linejoin", "round")
      .attr("stroke-linecap", "round")
      .attr("stroke-dasharray", `0,${d3dataLength}`)
      .attr("d", line)
    .transition()
      .duration(5000)
      .ease(d3.easeLinear)
      .attr("stroke-dasharray", '${d3dataLength},${d3dataLength}');

	return cb ? cb(res) : res;
};


