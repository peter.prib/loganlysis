
import WebGL from 'three/addons/capabilities/WebGL.js';
if( WebGL.isWebGLAvailable() ) {
    animate();
} else {
  document.getElementById( 'container' ).appendChild(WebGL.getWebGLErrorMessage());
}


import * as THREE from 'three';
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );

const renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

const geometry = new THREE.BoxGeometry( 1, 1, 1 );
const material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
const cube = new THREE.Mesh( geometry, material );
scene.add( cube );

camera.position.z = 5;

function drawObject() {
const geometry = new THREE.BufferGeometry();
const material = new THREE.MeshBasicMaterial( { color: 0xff0000 } );
const vertices = new Float32Array( [
-1.0, -1.0,  1.0, // v0
 1.0, -1.0,  1.0, // v1
] );
geometry.setAttribute( 'position', new THREE.BufferAttribute( vertices, 3 ) );
const LineLoop = new THREE.LineLoop(geometry, material : Material )
}

function animate() {
requestAnimationFrame( animate );
cube.rotation.x += 0.01;
cube.rotation.y += 0.01;
renderer.render( scene, camera );
}

//animate();