alasql.getElement = function (selector) {
	if (!selector) throw new Error('Selected HTML element not specified');
	if (typeof selector !== "string") return selector;
	const selectedElememt = document.querySelector(selector);
	if (!selectedElememt) throw new Error('Selected HTML element is not found for ' + selector);
	return selectedElememt
}

alasql.htmlTableElement = function (data, opts, columns) {
	if (columns.length === 0) {
		if (typeof data[0] === 'object') {
			columns = Object.keys(data[0]).map(function (columnid) {
				return { columnid: columnid };
			});
		} else {
			// What should I do?
			// columns = [{columnid:"_"}];
		}
	}
	const tbe = document.createElement('table');
	const thead = document.createElement('thead');
	tbe.appendChild(thead);
	if (opts.headers) {
		const tre = document.createElement('tr');
		for (let i = 0; i < columns.length; i++) {
			let the = document.createElement('th');
			the.textContent = columns[i].columnid;
			tre.appendChild(the);
		}
		thead.appendChild(tre);
	}
	const tbody = document.createElement('tbody');
	tbe.appendChild(tbody);
	for (let j = 0; j < data.length; j++) {
		const tre = document.createElement('tr');
		for (let i = 0; i < columns.length; i++) {
			const the = document.createElement('td');
			the.textContent = data[j][columns[i].columnid];
			tre.appendChild(the);
		}
		tbody.appendChild(tre);
	}
	return tbe
}
alasql.into.HTML = function (selector, opts, data, columns, cb) {
	let res = 1;
	const selectedElememt = alasql.getElement(selector)
	const opt = { headers: true };
	alasql.utils.extend(opt, opts);
	alasql.utils.domEmptyChildren(selectedElememt);
	selectedElememt.appendChild(alasql.htmlTableElement(data, opts, columns));
	return cb ? cb(res) : res;
};
alasql.into.VISJSGRAPH = function (graph, opts, data, columns, cb) {
	if (!graph) throw Error("no graph target")
	if (!data) throw Error("no data")
	if (data.length == 0) throw Error("no data")
	let res = 1;
	const dataset = new vis.DataSet(data);
	if(typeof graph == 'array') {
		graph.forEach(g=>{
			if(g.camera){
				g.setData(dataset)
				return
			}
			if(g.filter){
				const dataView = new vis.DataView(dataset, {filter: g.filter})
				g.graph.setData(dataView.get())
				return
			} 
			g.graph.setData(dataset)
		})
	} else if(typeof graph == 'object'){
		const g=graph[id];
		if(graph.camera){
			graph.setData(dataset)
			return
		}
		if(graph.filter){
			const dataView = new vis.DataView(dataset, {filter: graph.filter})
			graph.setData(dataView.get())
			return
		} 
		graph.setData(dataset)
	} else if(typeof graph == 'object')
		Object.keys(graph).forEach(id => {
			const g=graph[id];
			if(g.camera){
				g.setData(dataset)
				return
			}
			if(g.filter){
				const dataView = new vis.DataView(dataset, {filter: g.filter})
				g.graph.setData(dataView.get())
				return
			} 
			g.graph.setData(dataset)
		})
	else
	  graph.setData(dataset)
	return cb ? cb(res) : res;
};
alasql.into.HOT = function (selector, opts, data, columns, cb) {
	if (!selector) throw Error("no graph target")
	if (!data) throw Error("no data")
	if (data.length == 0) throw Error("no data")
	const arrayData = alasql.tableData2ArrayWithHeader(data, opts, columns)
	const container = alasql.getElement(selector)
	const hot = new Handsontable(container, {
		data: arrayData,
		rowHeaders: true,
		colHeaders: true,
		height: 'auto',
		licenseKey: 'non-commercial-and-evaluation' // for non-commercial use only
	});
	return cb ? cb(res) : res;
};

alasql.tableData2CSV = function (data, opts) {
	const opt = { headers: true, separator: ';', quote: '"', utf8Bom: true };
	if (opts && !opts.headers && typeof opts.headers !== 'undefined') {
		opt.utf8Bom = false;
	}
	alasql.utils.extend(opt, opts, columns);
	const s = opt.utf8Bom ? '\ufeff' : '';
	if (opt.headers) {
		s += opt.quote +
			columns.map((col) => col.columnid.trim())
				.join(opt.quote + opt.separator + opt.quote) +
			opt.quote +
			'\r\n';
	}

	data.forEach(function (d) {
		s +=
			columns
				.map(function (col) {
					let s = d[col.columnid];
					// escape the character wherever it appears in the field
					if (opt.quote !== '') {
						s = (s + '').replace(new RegExp('\\' + opt.quote, 'g'), opt.quote + opt.quote);
					}
					//			if((s+"").indexOf(opt.separator) > -1 || (s+"").indexOf(opt.quote) > -1) s = opt.quote + s + opt.quote;

					//Excel 2013 needs quotes around strings - thanks for _not_ complying with RFC for CSV
					if (+s != s) {
						// jshint ignore:line
						s = opt.quote + s + opt.quote;
					}
					return s;
				})
				.join(opt.separator) + '\r\n';
	});
}
alasql.tableData2ArrayWithHeader = function (data, opts, columns) {
	const opt = { headers: true };
	alasql.utils.extend(opt, opts);
	if (!columns) throw Error("missing colums")
	const resultArray = [];
	if (opt.headers) {
		resultArray.push(columns.map((col) => col.columnid.trim()))
	}

	data.forEach((d) => {
		const row = columns.map((col)=>{
			const s = d[col.columnid];
			return s ? s : null;
		})
	});
}

alasql.into.THREE = function (selector, opts, data, columns, cb) {
/* requires
 <script src="https://cdnjs.cloudflare.com/ajax/libs/three.js/0.147.0/three.min.js"></script>
  <script async src="https://unpkg.com/es-module-shims@1.6.0/dist/es-module-shims.js"></script>
    <script type="importmap">
        {
          "imports": {
            "three": "https://cdn.skypack.dev/three@0.144.0/build/three.module",
            "three/": "https://cdn.skypack.dev/three@0.144.0/"
          }
        }
    </script>
*/
	let res = 1;
	const selectedElememt = alasql.getElement(selector)
	const opt = {};
	alasql.utils.extend(opt, opts);
	alasql.utils.domEmptyChildren(selectedElememt);
	selectedElememt.appendChild(alasql.htmlTableElement(data, opts, columns));

//	import * as THREE from 'three';
//	import Stats from 'three/addons/libs/stats.module.js';
	let container, stats, clock, camera, scene, renderer, line;
	const segments = 10000;
	const r = 800;
	let t = 0;

	container = document.getElementById( 'container' );
	camera = new THREE.PerspectiveCamera( 27, window.innerWidth / window.innerHeight, 1, 4000 );
	camera.position.z = 2750;
	scene = new THREE.Scene();
	clock = new THREE.Clock();
	const geometry = new THREE.BufferGeometry();
	const material = new THREE.LineBasicMaterial( { vertexColors: true } );
	const positions = [];
	const colors = [];
	for ( let i = 0; i < segments; i ++ ) {
		const x = Math.random() * r - r / 2;
		const y = Math.random() * r - r / 2;
		const z = Math.random() * r - r / 2;
		positions.push( x, y, z );
		colors.push( ( x / r ) + 0.5 );
		colors.push( ( y / r ) + 0.5 );
		colors.push( ( z / r ) + 0.5 );
	}
	geometry.setAttribute( 'position', new THREE.Float32BufferAttribute( positions, 3 ) );
	geometry.setAttribute( 'color', new THREE.Float32BufferAttribute( colors, 3 ) );
	generateMorphTargets( geometry );
	geometry.computeBoundingSphere();
	line = new THREE.Line( geometry, material );
	scene.add( line );

	renderer = new THREE.WebGLRenderer();
	renderer.setPixelRatio( window.devicePixelRatio );
	renderer.setSize( window.innerWidth, window.innerHeight );

	container.appendChild( renderer.domElement );

	stats = new Stats();
	container.appendChild( stats.dom );

	window.addEventListener( 'resize', onWindowResize );
	requestAnimationFrame( animate );
	render();
	stats.update();
	return cb ? cb(res) : res;
	function onWindowResize() {
		camera.aspect = window.innerWidth / window.innerHeight;
		camera.updateProjectionMatrix();
		renderer.setSize( window.innerWidth, window.innerHeight );
	}
	function render() {
		const delta = clock.getDelta();
		const time = clock.getElapsedTime();
		line.rotation.x = time * 0.25;
		line.rotation.y = time * 0.5;
		t += delta * 0.5;
		line.morphTargetInfluences[ 0 ] = Math.abs( Math.sin( t ) );
		renderer.render( scene, camera );
	}
	function generateMorphTargets( geometry ) {
		const data = [];
		for ( let i = 0; i < segments; i ++ ) {
			const x = Math.random() * r - r / 2;
			const y = Math.random() * r - r / 2;
			const z = Math.random() * r - r / 2;
			data.push( x, y, z );
		}
		const morphTarget = new THREE.Float32BufferAttribute( data, 3 );
		morphTarget.name = 'target1';
		geometry.morphAttributes.position = [ morphTarget ];
	}
};
alasql.targetGraphs = function (data,graph,setOptions=()=>null,dataMapping=(row)=>row) {
	if (!data) throw Error("no data")
	if(typeof graph == 'array') {
		graph.forEach(g=>{
			this.targetGraphs(g,setOptions)
		})
	} else if(typeof graph == 'object')
		Object.keys(graph).forEach(id => {
			this.targetGraphs(graph[id],setOptions);
		})
	else
	  setData(data)
}
alasql.into.ECHART = function (targetContainer, opts, data, columns, cb) {
	if (!data) throw Error("no data")
	if (!targetContainer) throw Error("no target container")
	const ranges=data.reduce((a,c)=>[Math.min(a[0].c.x),Math.max(a[1],c.x),
									Math.min(a[2].c.x),Math.max(a[1],c.x),
									Math.min(a[6].c.x),Math.max(a[1],c.x)
								],[null,null,null,null,null,null,null])
	const optionDefault=Object.assign({
		dataMapping:(item)=> {return { value: [item.x, item.y, item.z]}},
		color: ['#313695', '#4575b4', '#74add1', '#abd9e9', '#e0f3f8', '#ffffbf', '#fee090', '#fdae61', '#f46d43',
			'#d73027', '#a50026'
	  		],
		xAxis:[ranges[0],ranges[1]],
		yAxis:[ranges[2],ranges[3]],
	},opts);
	const dom = typeof targetContainer == 'string' ? document.getElementById(targetContainer) : targetContainer;
	const myChart = echarts.init(dom, null, {
	  renderer: 'canvas',
	  useDirtyRect: false
	});
	const option = {
	  tooltip: {},
	  visualMap: {
		max: 20,
		inRange: {
			color:optionDefault.color
		}
	  },
	  xAxis3D: { type: 'category', data: optionDefault.xAxis},
	  yAxis3D: { type: 'category', data: optionDefault.yAxis},
	  zAxis3D: { type: 'value' },
	  grid3D: {
		boxWidth: 200,
		boxDepth: 80,
		light: {
		  main: {
			intensity: 1.2
		  },
		  ambient: {
			intensity: 0.3
		  }
		}
	  },
	  series: [
		{
		  type: 'bar3D',
		  data: data.map(dataMapping),
		  shading: 'color',
		  label: {
			show: false,
			fontSize: 16,
			borderWidth: 1
		  },
		  itemStyle: {
			opacity: 0.4
		  },
		  emphasis: {
			label: {
			  fontSize: 20,
			  color: '#900'
			},
			itemStyle: {
			  color: '#900'
			}
		  }
		}
	  ]
	};
	myChart.setOption(option);
	window.addEventListener('resize', myChart.resize);
}